package de.dhbw.cheesyweather;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import de.dhbw.cheesyweather.util.IconUtil;
import de.dhbw.cheesyweather.util.ThemeUtil;
import de.dhbw.cheesyweather.weather.Language;
import de.dhbw.cheesyweather.weather.WeatherTypes;
import de.dhbw.cheesyweather.weather.beans.AbstractForecastDataBean;
import de.dhbw.cheesyweather.weather.beans.CityBean;
import de.dhbw.cheesyweather.weather.beans.DayBean;
import de.dhbw.cheesyweather.weather.beans.ForecastBean;
import de.dhbw.cheesyweather.weather.tasks.CityCodeTask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import de.dhbw.cheesyweather.util.Cache;
import de.dhbw.cheesyweather.weather.tasks.GeocodingCallback;
import de.dhbw.cheesyweather.weather.tasks.GeocodingTask;
import de.dhbw.cheesyweather.weather.tasks.WeatherTask;
import de.dhbw.cheesyweather.weather.text.WeatherTextDecider;
import de.dhbw.cheesyweather.weather.text.WeatherTextDecision;


/**
 * Implementation of App Widget functionality.x
 */
public class WeatherWidget extends AppWidgetProvider {

    private static String W = "w";
    private static String H = "h";

    private WeatherTextDecider weatherText;
    private LocationManager locationManager;

    /**
     * Called when android wants to update the widgets. Receives a integer array with the appWidget Ids
     *
     * @param context
     * @param appWidgetManager
     * @param appWidgetIds
     */
    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Cache.context = context;

        Log.d("WeatherWidget", "update");

        final List<Integer> gpsWidgetIds = new ArrayList<Integer>();

        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            if (!Settings.getWasCreated(context, appWidgetIds[i])) {
                Log.i("WeatherWidget#" + appWidgetIds[i], "Widget was not yet created, skipping update");
                continue;
            }
            if (Settings.getUseGps(context, appWidgetIds[i])) {
                gpsWidgetIds.add(appWidgetIds[i]);
            } else {
                updateAppWidget(context, appWidgetManager, appWidgetIds[i], Settings.getCity(context, appWidgetIds[i]));
            }
        }

        if (this.locationManager == null) {
            this.locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("WeatherWidget", "Location update!");
                GeocodingTask geocoding = new GeocodingTask();
                geocoding.setCallback(new GeocodingCallback() {
                    @Override
                    public void run(String cityName) {
                        Log.d("WeatherWidget", "received City: " + cityName);
                        for (int appWidgetId : gpsWidgetIds) {
                            updateAppWidget(context, appWidgetManager, appWidgetId, cityName);
                        }
                    }
                });
                geocoding.execute(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("WeatherWidget", "Location Provider disabled");
            }
        };

        this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationListener.onLocationChanged(this.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
    }

    /**
     * Updates a widget using a given appWidgetId and the name of the city to update with
     *
     * @param context
     * @param appWidgetManager
     * @param appWidgetId
     * @param cityName
     */
    private void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager, final int appWidgetId, final String cityName) {
        if (this.weatherText == null) {
            this.weatherText = initWeatherText(context);
        }

        Bundle bundle = appWidgetManager.getAppWidgetOptions(appWidgetId);
        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_widget);

        ThemeUtil.setColorsFromSettings(views, appWidgetId);

        // first of all we update the responsive elements of the view
        this.doResponsiveVisibility(views, bundle);
        appWidgetManager.updateAppWidget(appWidgetId, views);

        Log.d("WeatherWidget", "updateAppWidget - usegps is " + (Settings.getUseGps(context, appWidgetId) ? "true" : "false"));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // get City code
                    AsyncTask codeTask = new CityCodeTask().execute(cityName);
                    List<CityBean> cities = (List<CityBean>) codeTask.get();

                    // get forecast for given City
                    AsyncTask weatherTask = new WeatherTask(Language.GERMAN).execute(cities.get(0).getCityCode());
                    ForecastBean forecast = (ForecastBean) weatherTask.get();

                    // get main Icon and format temperature
                    String temp = forecast.getDays().get(0).getAverageTemperature() + " °C";
                    Bitmap icon = IconUtil.getIcon(forecast.getDays().get(0).getWeatherCode(), true, Settings.getForeground(context, appWidgetId));

                    // set main icon and text
                    views.setTextViewText(R.id.weather_currenttemp, temp);
                    views.setImageViewBitmap(R.id.weather_icon, icon);
                    views.setTextViewText(R.id.weather_text, weatherText.getText(forecast.getDays().get(0)));

                    // set forecast fields
                    int forecastLength = forecast.getDays().size();
                    DateFormat shortDate = new SimpleDateFormat("E");
                    DayBean day;
                    switch (forecastLength) {
                        case 3:
                            day = forecast.getDays().get(2);
                            views.setTextViewText(R.id.weather_forecast_plus3day_name, shortDate.format(day.getDate()));
                            views.setTextViewText(R.id.weather_forecast_plus3day_temp, day.getAverageTemperature() + " °C");
                            views.setImageViewBitmap(
                                    R.id.weather_forecast_plus3day_icon,
                                    IconUtil.getIcon(day.getWeatherCode(), Settings.getForeground(context, appWidgetId))
                            );
                        case 2:
                            day = forecast.getDays().get(1);
                            views.setTextViewText(R.id.weather_forecast_plus2day_name, shortDate.format(day.getDate()));
                            views.setTextViewText(R.id.weather_forecast_plus2day_temp, day.getAverageTemperature() + " °C");
                            views.setImageViewBitmap(
                                    R.id.weather_forecast_plus2day_icon,
                                    IconUtil.getIcon(day.getWeatherCode(), Settings.getForeground(context, appWidgetId)));
                        case 1:
                            day = forecast.getDays().get(0);
                            views.setTextViewText(R.id.weather_forecast_plus1day_name, shortDate.format(day.getDate()));
                            views.setTextViewText(R.id.weather_forecast_plus1day_temp, day.getAverageTemperature() + " °C");
                            views.setImageViewBitmap(
                                    R.id.weather_forecast_plus1day_icon,
                                    IconUtil.getIcon(day.getWeatherCode(), Settings.getForeground(context, appWidgetId))
                            );
                    }


                    appWidgetManager.updateAppWidget(appWidgetId, views);

                    Log.d("Forecast", forecast.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    private WeatherTextDecider initWeatherText(Context context) {

        WeatherTextDecider weatherText = new WeatherTextDecider(
                context.getResources().getStringArray(R.array.weathertext_neutral)
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_sunny),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getAverageTemperature() > 30 && day.getCleanWeatherCode() == WeatherTypes.SUNNY;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_cloudy),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.CLOUDY ||
                                day.getCleanWeatherCode() == WeatherTypes.OVERCAST;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_partly_cloudy),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.PARTLY_COUDY;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_cold),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getAverageTemperature() < 10;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_snow),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.SNOW;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_fog),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.FOG;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_rainy),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.RAIN ||
                                day.getCleanWeatherCode() == WeatherTypes.LIGHT_RAIN;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_thunderstorm),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.THUNDERSTORM;
                    }
                }
        );

        weatherText.addSet(
                context.getResources().getStringArray(R.array.weathertext_drizzle),
                new WeatherTextDecision() {
                    @Override
                    public boolean decide(AbstractForecastDataBean day) {
                        return day.getCleanWeatherCode() == WeatherTypes.DRIZZLE;
                    }
                }
        );

        return weatherText;
    }

    /**
     * Called, when the options of the widget are changed (for example on resize)
     *
     * @param context
     * @param appWidgetManager
     * @param appWidgetId
     * @param bundle
     */
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager,
                                          int appWidgetId, Bundle bundle) {
        Cache.context = context;

        Log.d("WeatherWidget", "onAppWidgetOptionsChanged " + this.getWidgetSize(bundle).toString());
        Log.d("WeatherWidget", "onAppWidgetOptionsChanged - usegps is " + (Settings.getUseGps(context, appWidgetId) ? "true" : "false"));
        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_widget);

        this.doResponsiveVisibility(views, appWidgetManager.getAppWidgetOptions(appWidgetId));
        appWidgetManager.updateAppWidget(appWidgetId, views);

        int[] ints = {appWidgetId};
        this.onUpdate(context, appWidgetManager, ints);
    }

    /**
     * Depending on the size of the widget, we want to decide to show or hide certain elements
     * That is what happens here.
     * <p/>
     * Do not forget to call updateAppWidget after executing this function - if you do not do this,
     * the widget content will not update
     *
     * @param views  The RemoteViews instance used
     * @param bundle The bundle used to determine the size of the widget
     */
    private void doResponsiveVisibility(RemoteViews views, Bundle bundle) {
        Map<String, Integer> size = this.getWidgetSize(bundle);

        Log.d("WeatherWidget", size.toString());

        // we show the icon, if the width is > 260
        boolean iconVisible = size.get(W) > 260;
        Log.d("WeatherWidget", "iconandtemp is: " + (iconVisible ? "VISIBLE" : "GONE"));
        views.setViewVisibility(R.id.weather_iconandtemp, iconVisible ? View.VISIBLE : View.GONE);

        boolean forecastVisible = size.get(H) > 175 && size.get(W) > 240;

        
        Log.d("WeatherWidget", "forecast is: " + (forecastVisible ? "VISIBLE" : "GONE"));
        views.setViewVisibility(R.id.weather_forecast, forecastVisible ? View.VISIBLE : View.GONE);
    }

    /**
     * There is no real way of determining how large a widget is, but there is this hacky way.
     * According to the SO post linked below, this is the mapping of the bundle attributes:
     * - minwidth:      potrait width
     * - maxheight:     potrait height
     * - maxwidth:      landscape width
     * - minheight:     landscape height
     * <p/>
     * To be sure, that our widget is always displayed, we are using minwidth and minheight
     *
     * @return
     * @link http://stackoverflow.com/questions/14244984/determine-the-homescreens-appwidgets-space-grid-size
     */
    private Map<String, Integer> getWidgetSize(Bundle bundle) {
        Map<String, Integer> map = new HashMap<String, Integer>();

        map.put(W, bundle.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH));
        map.put(H, bundle.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT));

        return map;
    }

}


