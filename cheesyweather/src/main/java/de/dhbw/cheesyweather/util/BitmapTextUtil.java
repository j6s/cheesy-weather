package de.dhbw.cheesyweather.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * BitmapTextUtil:
 * - Utility to render Text on a Bitmap
 */
public class BitmapTextUtil {

    /**
     * Calls textToBitmap with a 200x200 size default canvas
     *
     * @param text
     * @param font
     * @param fontsize
     * @param verticallyCentered
     * @param color
     * @return
     */
    public static Bitmap textToBitmap(String text, Typeface font, int fontsize, boolean verticallyCentered, int color) {
        return textToBitmap(text, font, fontsize, 200, 200, color, verticallyCentered);
    }

    /**
     * Renders a bitmap from a given text
     *
     * @param text      The text to print into the bitmap
     * @param font      The font used. If the font is null, no font is used
     * @param fontsize  The fontsize to print with
     * @param width     The width of the resulting bitmap
     * @param height    The height of the resulting bitmap
     * @param color     The color to print the text with (the background is always transparent)
     * @param verticallyCentered    Wether or not we want to vertically Center the Text
     * @return
     */
    public static Bitmap textToBitmap(String text, Typeface font, int fontsize, int width, int height, int color, boolean verticallyCentered) {
        fontsize =  (int) MetricUtil.dp2px(fontsize);
        width =     (int) MetricUtil.dp2px(width);
        height =    (int) MetricUtil.dp2px(height);

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        paint.setSubpixelText(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color);
        paint.setTextSize(fontsize);
        paint.setTextAlign(Paint.Align.CENTER);

        if(font != null) {
            paint.setTypeface(font);
        }

        // calculate the text position
        int x = (width / 2);
        int y;
        if(verticallyCentered) {
            y = (int) (height / 2 + paint.getTextSize() / 2);
        }else{
            y = (int) (paint.getTextSize() * 1.2);
        }

        canvas.drawText(text, x, y, paint);

        return bitmap;
    }

}
