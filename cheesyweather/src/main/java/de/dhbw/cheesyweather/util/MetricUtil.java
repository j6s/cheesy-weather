package de.dhbw.cheesyweather.util;

import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Metric Util:
 * Android has lots of different units.
 * MetricUtil helps handle those
 */
public class MetricUtil {

    /**
     * Converts a dp unit to a pixel unit
     *
     * @param dp    The length in dp
     * @return      The length in px
     */
    public static float dp2px (float dp) {
        DisplayMetrics metrics = Cache.context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

}
