package de.dhbw.cheesyweather.util;

import android.graphics.Typeface;

import de.dhbw.cheesyweather.exceptions.FontNotFoundException;

/**
 * FontUtil: Handles the font handling
 */
public class FontUtil {

    /**
     * Name of the weather icon font
     */
    public static final String WEATHER_ICON = "weathericon.ttf";

    /**
     * Gets a font from a given asset name.
     * Uses Cache to cache the font, so that it does not have to be fetched again if the function
     * is called multipe times
     *
     * @param assetname
     * @return
     * @throws FontNotFoundException
     */
    public static Typeface getTypeface(String assetname) throws FontNotFoundException {
        if(Cache.hasFont(assetname)) {
            return Cache.getFont(assetname);
        }

        Typeface font = Typeface.createFromAsset(Cache.context.getAssets(), assetname);

        Cache.setFont(assetname, font);
        return font;
    }
}
