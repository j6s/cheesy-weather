package de.dhbw.cheesyweather.util;

/**
 * Represents a default theme
 */
public class Theme {

    private String name;
    private int foreground;
    private int background;

    public Theme(String name, int foreground, int background) {
        this.name = name;
        this.foreground = foreground;
        this.background = background;
    }

    public int getForeground() {
        return foreground;
    }

    public void setForeground(int foreground) {
        this.foreground = foreground;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
