package de.dhbw.cheesyweather.util;

import android.graphics.Bitmap;
import android.graphics.Typeface;

import de.dhbw.cheesyweather.exceptions.FontNotFoundException;
import de.dhbw.cheesyweather.weather.WeatherTypes;

public class IconUtil {

    /**
     * Gets the weather icon for one type of weather.
     *
     * @param weatherType The weatherCode from the weather.com API
     * @param large       Whether or not you want to have a large icon
     * @return The icon as bitmap
     */
    public static Bitmap getIcon(int weatherType, boolean large, int color) {
        // we only need the first number
        weatherType = Integer.parseInt(new Integer(weatherType).toString().substring(0, 1));
        String text = WeatherTypes.getIcon(weatherType);
        Typeface font;
        try {
            font = FontUtil.getTypeface(FontUtil.WEATHER_ICON);
        } catch (FontNotFoundException e) {
            font = null;
        }

        if (large) {
            return BitmapTextUtil.textToBitmap(
                    text,
                    font,
                    130,
                    false,
                    color
            );
        }

        return BitmapTextUtil.textToBitmap(
                text,
                font,
                60,
                true,
                color
        );
    }

    /**
     * Calls getIcon with the large bool set to false
     *
     * @param weatherType The weatherCode from the weather.com API
     * @return The icon as bitmap
     */
    public static Bitmap getIcon(int weatherType, int color) {
        return getIcon(weatherType, false, color);
    }
}
