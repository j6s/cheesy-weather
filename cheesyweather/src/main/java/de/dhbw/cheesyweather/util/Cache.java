package de.dhbw.cheesyweather.util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

import de.dhbw.cheesyweather.exceptions.FontNotFoundException;

/**
 * Cache: Used to cache certain values in order to use them later
 */
public class Cache {

    /**
     * FontCache, used by the FontUtil. This way we do not have to load a font multiple
     * times, if we request it multiple times
     */
    private static Map<String, Typeface> fontCache= new HashMap<>();

    /**
     * Checks, if the cache contains a certain font (identified by it's name)
     *
     * @param name  The name of the font to load
     * @return  Wether or not the font exists
     */
    public static boolean hasFont(String name) {
        return fontCache.containsKey(name);
    }

    /**
     * Gets a font or throws a FontNotFoundException, if a font was not found.
     *
     * @param name  The name of the font to load
     * @return  The font
     * @throws FontNotFoundException
     */
    public static Typeface getFont(String name) throws FontNotFoundException {
        if(!hasFont(name)) {
            throw new FontNotFoundException("Font " + name + " was not found");
        }
        return fontCache.get(name);
    }

    /**
     * Adds a font to the font Cache
     *
     * @param name Name of the font
     * @param font The font
     */
    public static void setFont(String name, Typeface font) {
        fontCache.put(name, font);
    }

    /**
     * Copy of the applicationContext. We save the copy here, in order to use it in Classes, that
     * would not normally have a context
     *
     * Also, it saves a lot of unnecessary function arguments, because without the context cache we
     * would have to pass the context as a argument through a lot of classes and functions.
     */
    public static Context context;
}
