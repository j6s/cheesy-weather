package de.dhbw.cheesyweather.util;

import android.widget.RemoteViews;

import de.dhbw.cheesyweather.R;
import de.dhbw.cheesyweather.Settings;

public class ThemeUtil {

    public static Theme[] themes = {
            new Theme("Black on White",             0xFF000000, 0xFFFFFFFF),
            new Theme("Black on White transparent", 0xFF000000, 0x99FFFFFF),
            new Theme("White on Black",             0xFFFFFFFF, 0xFF000000),
            new Theme("White on Black transparent", 0xFFFFFFFF, 0x99000000),
            new Theme("Solarized Bright",           0xFF073642, 0xFFeee8d5),
            new Theme("Solarized Dark",             0xFFeee8d5, 0xFF073642),
            new Theme("Transparent Black",          0xFF000000, 0x00000000),
            new Theme("Transparent White",          0xFFFFFFFF, 0x00000000),
            new Theme("Glass",                      0xFFFFFFFF, 0x55FFFFFF),
            new Theme("Holo",                       0xFF33b5e5, 0xaa333333)
    };

    public static String[] getThemeNames() {
        String[] retval = new String[themes.length];
        int i = 0;
        for(Theme theme : themes) {
            retval[i] = theme.getName();
            i++;
        }
        return retval;
    }

    public static void setColors(RemoteViews view, int foreGround, int backGround) {
        view.setInt(R.id.weather_widget, "setBackgroundColor", backGround);
        view.setInt(R.id.weather_currenttemp, "setTextColor", foreGround);
        view.setInt(R.id.weather_text, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus1day_name, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus1day_temp, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus2day_name, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus2day_temp, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus3day_name, "setTextColor", foreGround);
        view.setInt(R.id.weather_forecast_plus3day_temp, "setTextColor", foreGround);
    }

    public static void setColorsFromSettings(RemoteViews view, int appId) {
        setColors(
                view,
                Settings.getForeground(Cache.context, appId),
                Settings.getBackground(Cache.context, appId)
        );
    }

}
