package de.dhbw.cheesyweather.exceptions;

import java.io.FileNotFoundException;

public class FontNotFoundException extends FileNotFoundException {
    public FontNotFoundException() {}
    public FontNotFoundException(String detailMessage) {
        super(detailMessage);
    }
}
