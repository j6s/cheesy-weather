package de.dhbw.cheesyweather;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.Spinner;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;

import de.dhbw.cheesyweather.util.Theme;
import de.dhbw.cheesyweather.util.ThemeUtil;

/**
 * Settings activty:
 *
 * - Shown on creation of the widget
 * - Saves the settings in SharedPreferences
 * - Has public static methods to retrieve settings for specific widgets
 *
 * We are deviating from the naming convention of adding "Activity" to the class, because
 * calling the public static methods on Settings makes more sense.
 */
public class Settings extends Activity {

    /**
     * Constants used as settings keys
     * Note; The appWidgetId will be appended to these key in order to
     * have different configuration for multiple widgets
     */
    public static final String PREF_NAME = WeatherWidget.class.getCanonicalName();
    public static final String USE_GPS = "usegps";
    public static final String CITY = "city";
    public static final String WAS_CREATED = "was_created";
    public static final String FOREGROUND = "foreground";
    public static final String BACKGORUND = "background";
    public static final String THEME = "theme";

    /**
     * Called upon creation of the activity: All of the setup happens here
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // we set the cancelled result by default: If someone exits without saving, the
        // widget creation process should be stopped
        setResult(RESULT_CANCELED);

        Spinner themeSpinner = (Spinner) findViewById(R.id.pref_theme);
        Button saveButton = (Button) findViewById(R.id.pref_save);
        final ColorPicker foreground  = (ColorPicker) findViewById(R.id.pref_foreground);
        final ColorPicker background = (ColorPicker) findViewById(R.id.pref_background);

        // setting up the foreground color picker
        foreground.addOpacityBar((OpacityBar) findViewById(R.id.pref_foreground_opacity));
        foreground.addSaturationBar((SaturationBar) findViewById(R.id.pref_foreground_saturation));
        foreground.addValueBar((ValueBar) findViewById(R.id.pref_foreground_lightness));

        // setting up the background color picker
        background.addOpacityBar((OpacityBar) findViewById(R.id.pref_background_opacity));
        background.addSaturationBar((SaturationBar) findViewById(R.id.pref_background_saturation));
        background.addValueBar((ValueBar) findViewById(R.id.pref_background_lightness));

        // setting up the theme selector dropdown
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ThemeUtil.getThemeNames());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        themeSpinner.setAdapter(adapter);
        themeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            /**
             * Sets the colorpickers to the correct colors on change
             */
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Theme theme = ThemeUtil.themes[position];
                foreground.setColor(theme.getForeground());
                background.setColor(theme.getBackground());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // save configuration on click of the save button
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeConfiguration();
            }
        });
    }

    /**
     * @return  The id of the widget that called this settings dialog
     * @link https://developer.android.com/guide/topics/appwidgets/index.html
     */
    private int getAppWidgetId() {
        return getIntent().getExtras().getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID
        );
    }

    /**
     * Called, when the activity is being close.
     * We save all of our settings here
     */
    private void closeConfiguration() {
        int appWidgetId = this.getAppWidgetId();

        // update the view
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.weather_widget);
        appWidgetManager.updateAppWidget(appWidgetId, views);

        // get the fields
        CheckBox useGps = (CheckBox) findViewById(R.id.pref_usegps);
        EditText city = (EditText) findViewById(R.id.pref_city);
        ColorPicker foreground  = (ColorPicker) findViewById(R.id.pref_foreground);
        ColorPicker background = (ColorPicker) findViewById(R.id.pref_background);

        // set the settings
        setUseGps(this, appWidgetId, useGps.isEnabled());
        setCity(this, appWidgetId, city.getText().toString());
        setWasCreated(this, appWidgetId, true);
        setForeground(this, appWidgetId, foreground.getColor());
        setBackground(this, appWidgetId, background.getColor());

        // finish with a clean result
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    /**
     * gtes the shared preferences for a given context
     * @param context   The context to use in order to retrieve the SharedPreferences
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getApplicationContext().getSharedPreferences(PREF_NAME, 0);
    }

    /**
     * =========================================================================================
     * Notice: There will only be getters and setters for the SharedPreferences after this point.
     */

    /**
     * Sets the "usegps" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @param property      The property to set
     */
    public static void setUseGps(Context context, int appWidgetId, boolean property) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(USE_GPS + appWidgetId, property);
        editor.apply();
    }

    /**
     * Gets the "usegps" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @return The usegps setting or true if not set
     */
    public static boolean getUseGps(Context context, int appWidgetId) {
        return getSharedPreferences(context).getBoolean(USE_GPS + appWidgetId, true);
    }

    /**
     * Sets the "city" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @param property      The property to set
     */
    public static void setCity(Context context, int appWidgetId, String property) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CITY + appWidgetId, property);
        editor.apply();
    }

    /**
     * Gets the "city" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @return The usegps setting or "minseln" if not set
     * @TODO Have sensible defaults. Minseln is not what most people would want
     */
    public static String getCity(Context context, int appWidgetId) {
        String city = getSharedPreferences(context).getString(CITY + appWidgetId, "minseln");

        if(city == "") {
            return "minseln";
        }

        return city;
    }

    /**
     * Sets the "wascreated" setting for a specific widget, which is used to signalise the
     * widget that it was created and the initial settings were set
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @param property      The property to set
     */
    public static void setWasCreated(Context context, int appWidgetId, boolean property) {
        Log.d("Settings.set", WAS_CREATED + appWidgetId + "=" + (property ? "true" : "false"));
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(WAS_CREATED + appWidgetId, property);
        editor.apply();
    }

    /**
     * Gets the "wascreated" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @return The wascreated setting or false if not set
     */
    public static boolean getWasCreated(Context context, int appWidgetId) {
        boolean property = getSharedPreferences(context).getBoolean(WAS_CREATED + appWidgetId, false);
        Log.d("Settings.get", WAS_CREATED + appWidgetId + "=" + (property ? "true" : "false"));
        return property;
    }

    /**
     * Sets the "foreground" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @param property      The property to set
     */
    public static void setForeground(Context context, int appWidgetId, int property) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(FOREGROUND + appWidgetId, property);
        editor.apply();
    }

    /**
     * Gets the "foreground" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @return The foreground setting or black if not set
     */
    public static int getForeground(Context context, int appWidgetId) {
        return getSharedPreferences(context).getInt(FOREGROUND + appWidgetId, Color.BLACK);
    }

    /**
     * Sets the "background" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @param property      The property to set
     */
    public static void setBackground(Context context, int appWidgetId, int property) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(BACKGORUND + appWidgetId, property);
        editor.apply();
    }

    /**
     * Gets the "background" setting for a specific widget
     *
     * @param context       The context to use in order to retrieve the settings
     * @param appWidgetId   The id of the widget
     * @return The background setting or white if not set
     */
    public static int getBackground(Context context, int appWidgetId) {
        return getSharedPreferences(context).getInt(BACKGORUND + appWidgetId, Color.WHITE);
    }
}
