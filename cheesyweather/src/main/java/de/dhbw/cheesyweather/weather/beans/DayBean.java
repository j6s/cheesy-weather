package de.dhbw.cheesyweather.weather.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * DayBean:
 * Contains the average weather for the entire day and more
 * accurate weather for for time segments
 */
public class DayBean extends AbstractForecastDataBean {
    private Date date;
    private List<TimeBean> times;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<TimeBean> getTimes() {
        return times;
    }

    public void setTimes(List<TimeBean> times) {
        this.times = times;
    }

    public void addTime(TimeBean time){
        if(times == null){
            times = new ArrayList<>();
        }
        times.add(time);
    }

    @Override
    public String toString() {
        return "DayBean{" +super.toString()+
                ", date=" + date +
                ", times=" + times +
                '}';
    }
}
