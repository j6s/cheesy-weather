package de.dhbw.cheesyweather.weather.beans;

/*
 *AbstractForecastDataBean:
 * Contains the majority of the actual weather data because
 * DayBean and TimeBean have mostly the same fields
 */
public abstract class AbstractForecastDataBean {
    protected int forecastValidity;

    //from 0 to 999 s. http://www.wetter.com/apps_und_mehr/website/api/dokumentation/
    protected int weatherCode;
    protected String weatherText;
    protected int minimumTemperature;
    protected int maximumTemperature;

    //from 0 to 100
    protected int rainProbability;
    protected String windSpeed;

    //from 0 to 360
    protected int windDirectionDegrees;
    protected String windDirectionText;

    public int getForecastValidity() {
        return forecastValidity;
    }

    public void setForecastValidity(int forecastValidity) {
        this.forecastValidity = forecastValidity;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public int getCleanWeatherCode() {
        return Integer.parseInt(new Integer(weatherCode).toString().substring(0,1));
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public int getMinimumTemperature() {
        return minimumTemperature;
    }

    public void setMinimumTemperature(int minimumTemperature) {
        this.minimumTemperature = minimumTemperature;
    }

    public int getMaximumTemperature() {
        return maximumTemperature;
    }

    public void setMaximumTemperature(int maximumTemperature) {
        this.maximumTemperature = maximumTemperature;
    }

    public int getRainProbability() {
        return rainProbability;
    }

    public void setRainProbability(int rainProbability) {
        this.rainProbability = rainProbability;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getWindDirectionDegrees() {
        return windDirectionDegrees;
    }

    public void setWindDirectionDegrees(int windDirectionDegrees) {
        this.windDirectionDegrees = windDirectionDegrees;
    }

    public String getWindDirectionText() {
        return windDirectionText;
    }

    public void setWindDirectionText(String windDirectionText) {
        this.windDirectionText = windDirectionText;
    }

    public int getAverageTemperature(){
        return (minimumTemperature+maximumTemperature)/2;
    }

    @Override
    public String toString() {
        return  "forecastValidity='" + forecastValidity + '\'' +
                ", weatherCode='" + weatherCode + '\'' +
                ", weatherText='" + weatherText + '\'' +
                ", minimumTemperature='" + minimumTemperature + '\'' +
                ", maximumTemperature='" + maximumTemperature + '\'' +
                ", rainProbability='" + rainProbability + '\'' +
                ", windSpeed='" + windSpeed + '\'' +
                ", windDirectionDegrees='" + windDirectionDegrees + '\'' +
                ", windDirectionText='" + windDirectionText + '\'';
    }
}
