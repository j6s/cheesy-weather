package de.dhbw.cheesyweather.weather;

public enum Language {
    ENGLISH,
    GERMAN,
    SPANISH;
}
