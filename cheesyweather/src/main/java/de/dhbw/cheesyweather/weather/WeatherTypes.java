package de.dhbw.cheesyweather.weather;


import android.content.res.Resources;

import de.dhbw.cheesyweather.R;
import de.dhbw.cheesyweather.util.Cache;

/**
 * Types of weather:
 * @link http://www.wetter.com/apps_und_mehr/website/api/dokumentation/
 */
public class WeatherTypes {

    public static final int SUNNY = 0;
    public static final int PARTLY_COUDY = 1;
    public static final int CLOUDY = 2;
    public static final int OVERCAST = 3;
    public static final int FOG = 4;
    public static final int DRIZZLE = 5;
    public static final int RAIN = 6;
    public static final int SNOW = 7;
    public static final int LIGHT_RAIN = 8;
    public static final int THUNDERSTORM = 9;

    // the icons used for these types
    private static final Integer[] ICON_MAP = {
            R.string.icon_sunny,
            R.string.icon_partly_cloudy,
            R.string.icon_cloudy,
            R.string.icon_overcast,
            R.string.icon_fog,
            R.string.icon_drizzle,
            R.string.icon_rain,
            R.string.icon_snow,
            R.string.icon_light_rain,
            R.string.icon_thunderstorm
    };

    /**
     * Returns the correct Icon for the given Weather type
     * @param type
     * @return
     */
    public static String getIcon(int type) {
        if(type <= ICON_MAP.length) {
            return Cache.context.getString(ICON_MAP[type]);
        }

        throw new Resources.NotFoundException("Icon " + type + "was not found");
    }
}