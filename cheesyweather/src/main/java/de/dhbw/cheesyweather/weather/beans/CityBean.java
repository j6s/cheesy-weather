package de.dhbw.cheesyweather.weather.beans;

public class CityBean{
    private String cityCode;
    private String postCode;
    private String cityName;
    private String district;
    private String countryCode;
    private String countryName;
    private String state;
    private String region;

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "CityBean{" +
                "cityCode='" + cityCode + '\'' +
                ", postCode='" + postCode + '\'' +
                ", cityName='" + cityName + '\'' +
                ", district='" + district + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                ", state='" + state + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
