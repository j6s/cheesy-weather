package de.dhbw.cheesyweather.weather.tasks;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import de.dhbw.cheesyweather.util.Cache;

/**
 * Geocoding task:
 * Takes a Location and uses the Google reverse geocoding API to obtain Human readable
 * Information about the location
 */
public class GeocodingTask extends AsyncTask<Location, Void, String> {

    /**
     * The callback used after geocoding was successfull
     */
    private GeocodingCallback callback;

    /**
     * Setter for the callback
     * @param callback
     */
    public void setCallback(GeocodingCallback callback) {
        this.callback = callback;
    }

    /**
     * Handles the reverse geocoding API. Please only pas one location
     *
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(Location... params) {

        if(params.length == 0 || params[0] == null) {
            Log.e("GeocodingTask", "no Location given");
            return null;
        }
        Location location = params[0];

        Log.d("GeocodingTask", "Location: "+location.toString());

        Geocoder geocoder = new Geocoder(Cache.context);
        List<Address> adresses = null;
        try {
            adresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(adresses.size() == 0) {
            return null;
        }

        return adresses.get(0).getLocality();
    }

    /**
     * Called when doInBackground is ready. Executes the provided Callback
     *
     * @param result
     */
    protected void onPostExecute(String result) {
        Log.d("GeocodingTask", "onPostExecute " + result);
        if(this.callback != null && result != null) {
            this.callback.run(result);
        }
    }
}
