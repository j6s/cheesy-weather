package de.dhbw.cheesyweather.weather.text;

import de.dhbw.cheesyweather.weather.beans.AbstractForecastDataBean;

/**
 * Interface for decision used in WeatherText
 */
public interface WeatherTextDecision {

    boolean decide(AbstractForecastDataBean day);

}
