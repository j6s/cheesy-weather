package de.dhbw.cheesyweather.weather.tasks;

/**
 * GeocodingCallback: Simply callback interface used for the GeocodingTask
 */
public interface GeocodingCallback {
    void run(String cityName);
}
