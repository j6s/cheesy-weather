package de.dhbw.cheesyweather.weather.text;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.dhbw.cheesyweather.weather.beans.AbstractForecastDataBean;

/**
 * WeatherTextDecider:
 * Holds many instances of WeatherText and gets texts for the current weather
 */
public class WeatherTextDecider {

    /**
     * The weather texts
     */
    private List<WeatherText> weatherTexts = new ArrayList<>();

    /**
     * An array of neutral texts, which is used if none of the given weatherTexts have
     * applicable texts
     */
    private String[] neutrals;

    public WeatherTextDecider(String[] neutrals) {
        this.neutrals = neutrals;
    }

    /**
     * Creates a weatherText from the two arguments and adds it to this WeatherTextDecider
     * @param texts     The texts
     * @param decision  The decision callback
     */
    public void addSet(String[] texts, WeatherTextDecision decision) {
        this.addSet(new WeatherText(texts, decision));
    }

    /**
     * Adds a WeatherText to this instance
     * @param text
     */
    public void addSet(WeatherText text) {
        this.weatherTexts.add(text);
    }

    /**
     * Gets the text from the weatherTexts.
     * The first return value of WeatherText#getText which is not null is used.
     * If all of the WeatherTexts return null, a random element from this.neutrals is used
     *
     * @param data
     * @return
     */
    public String getText(AbstractForecastDataBean data) {
        String retval;

        for(WeatherText text : this.weatherTexts) {
            retval = text.getText(data);
            if(retval != null) {
                return retval;
            }
        }

        Log.w("WeatherTextDecider", "Nothing found for forecast, using neutral text " + data.toString());
        return this.neutrals[(int)Math.round(Math.random() * (this.neutrals.length - 1))];
    }

}
