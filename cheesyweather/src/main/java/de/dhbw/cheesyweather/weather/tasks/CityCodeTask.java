package de.dhbw.cheesyweather.weather.tasks;

import android.os.AsyncTask;
import android.util.Log;
import de.dhbw.cheesyweather.weather.ApiHelper;
import de.dhbw.cheesyweather.weather.beans.CityBean;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * CityCodeTask:
 * Gets the weather.com city ID from a given city name.
 */
public class CityCodeTask extends AsyncTask<String, Void, List<CityBean>> {

    @Override
    protected List<CityBean> doInBackground(String... cityName) {
        ApiHelper helper= new ApiHelper();
        JSONParser parser=new JSONParser();
        String singleCityName= cityName[0];

        URL url = null;
        try {
        url = new URL("http://api.wetter.com/location/name/search/"+ singleCityName + "/project/"
                + ApiHelper.PROJECT_NAME + "/cs/" + helper.generateChecksum(singleCityName) + "?output=json");
            Log.d("CityCodeTask", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject result = null;
        try{
            Object obj=parser.parse(new InputStreamReader(urlConnection.getInputStream()));
            result = (JSONObject) obj;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        if(result != null){
            List<CityBean> cities = new ArrayList<>();
            List<Map> resultList = (List<Map>) ((Map)result.get("search")).get("result");
            for(Map resultMap : resultList){
                CityBean city = new CityBean();
                city.setCityCode(resultMap.get("city_code").toString());
                city.setCityName(resultMap.get("name").toString());
                city.setPostCode(resultMap.get("plz").toString());
                city.setDistrict(resultMap.get("quarter").toString());
                city.setCountryCode(resultMap.get("adm_1_code").toString());
                city.setCountryName(resultMap.get("adm_1_name").toString());
                city.setState(resultMap.get("adm_2_name").toString());
                city.setRegion(resultMap.get("adm_4_name").toString());
                cities.add(city);
            }
            return cities;
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<CityBean> result) {
        Log.d("CityCodeTask", result.toString());
    }
}
