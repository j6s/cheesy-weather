package de.dhbw.cheesyweather.weather;

import android.util.Log;
import de.dhbw.cheesyweather.R;
import de.dhbw.cheesyweather.util.Cache;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Weather.com API Helper
 */
public class ApiHelper {

    /**
     * Name of the current project
     */
    public static final String PROJECT_NAME = Cache.context.getString(R.string.api_projectname);

    /**
     * API Key of the project
     */
    private static final String API_KEY = Cache.context.getString(R.string.api_key);

    /**
     * Generate the checksum for the weather.com API
     * @param variable
     * @return
     */
    public String generateChecksum(String variable){
        String data = PROJECT_NAME + API_KEY + variable;
        String checksum = "";
        try {
            byte[] dataBytes = data.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] checksumBytes = md.digest(dataBytes);
            checksum = String.format("%032X", new BigInteger(1, checksumBytes)).toLowerCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Log.d("ApiHelper", checksum);

        return checksum;
    }

}
