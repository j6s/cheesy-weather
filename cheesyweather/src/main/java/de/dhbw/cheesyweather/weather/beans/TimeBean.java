package de.dhbw.cheesyweather.weather.beans;

import java.sql.Time;

public class TimeBean extends AbstractForecastDataBean {
    private Time time;

    public TimeBean() {
        super();
    }

    public TimeBean(Time time) {
        super();
        this.time = time;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TimeBean{" + super.toString()+
                ", time=" + time +
                '}';
    }
}
