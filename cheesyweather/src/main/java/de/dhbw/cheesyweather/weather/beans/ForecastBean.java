package de.dhbw.cheesyweather.weather.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * ForecastBean:
 * Contains the days (up to three because of wetter.com api limitations)
 */
public class ForecastBean {
    private List<DayBean> days;

    public List<DayBean> getDays() {
        return days;
    }

    public void setDays(List<DayBean> days) {
        this.days = days;
    }

    public void addDay (DayBean day){
        if(days==null){
            days=new ArrayList<>();
        }
        days.add(day);
    }

    @Override
    public String toString() {
        return "ForecastBean{" +
                "days=" + days +
                '}';
    }
}
