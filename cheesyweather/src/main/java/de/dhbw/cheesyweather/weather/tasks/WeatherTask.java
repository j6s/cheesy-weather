package de.dhbw.cheesyweather.weather.tasks;

import android.os.AsyncTask;
import android.util.Log;
import de.dhbw.cheesyweather.weather.ApiHelper;
import de.dhbw.cheesyweather.weather.Language;
import de.dhbw.cheesyweather.weather.beans.AbstractForecastDataBean;
import de.dhbw.cheesyweather.weather.beans.DayBean;
import de.dhbw.cheesyweather.weather.beans.ForecastBean;
import de.dhbw.cheesyweather.weather.beans.TimeBean;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * WeatherTask:
 * Obtains the weather data for a given citycode from wetter.com and
 * returns it in a nicer java representation
 */
public class WeatherTask extends AsyncTask<String, Void, ForecastBean> {

    private final Language language;

    public WeatherTask(Language language) {
        this.language = language;
    }

    private <T extends AbstractForecastDataBean> T setData(T dataBean, Map dataMap){
        dataBean.setForecastValidity(Integer.parseInt(dataMap.get("p").toString()));
        dataBean.setWeatherCode(Integer.parseInt(dataMap.get("w").toString()));
        dataBean.setWeatherText(dataMap.get("w_txt").toString());
        dataBean.setMinimumTemperature(Integer.parseInt(dataMap.get("tn").toString()));
        dataBean.setMaximumTemperature(Integer.parseInt(dataMap.get("tx").toString()));
        dataBean.setRainProbability(Integer.parseInt(dataMap.get("pc").toString()));
        dataBean.setWindSpeed(dataMap.get("ws").toString());
        dataBean.setWindDirectionDegrees(Integer.parseInt(dataMap.get("wd").toString()));
        dataBean.setWindDirectionText(dataMap.get("wd_txt").toString());

        return dataBean;
    }

    @Override
    protected ForecastBean doInBackground(String... cityCode) {
        ApiHelper helper= new ApiHelper();
        JSONParser parser=new JSONParser();
        String languageParameter;

        switch (language){
            case ENGLISH: languageParameter="en";
                break;
            case SPANISH: languageParameter="es";
                break;
            case GERMAN: languageParameter="de";
                break;
            default: languageParameter="en";
        }

        URL url = null;
        try {
            url = new URL("http://api.wetter.com/forecast/weather/city/"+ cityCode[0] + "/project/"
                    + ApiHelper.PROJECT_NAME + "/cs/" + helper.generateChecksum(cityCode[0])
                    + "?output=json&language=" + languageParameter);
            Log.d("WeatherTask", url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject result = null;
        try{
            Object obj=parser.parse(new InputStreamReader(urlConnection.getInputStream()));
            result = (JSONObject) obj;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        if(result != null){
            ForecastBean forecast = new ForecastBean();
            Map<String, Object> forecastMap = (Map) ((Map)result.get("city")).get("forecast");
            String[] dateArr= forecastMap.keySet().toArray(new String [forecastMap.keySet().size()]);
            Arrays.sort(dateArr);
            for (String dateKey:dateArr){
                Map dayMap = (Map) forecastMap.get(dateKey);
                DayBean day = new DayBean();
                try {
                    day.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateKey));
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                day = setData(day, dayMap);
                day.addTime(setData(new TimeBean(new Time(5,0,0)), (Map) dayMap.get("06:00")));
                day.addTime(setData(new TimeBean(new Time(10,0,0)), (Map) dayMap.get("11:00")));
                day.addTime(setData(new TimeBean(new Time(16,0,0)), (Map) dayMap.get("17:00")));
                day.addTime(setData(new TimeBean(new Time(22,0,0)), (Map) dayMap.get("23:00")));
                forecast.addDay(day);
            }

            return forecast;
        }

        return null;
    }

    @Override
    protected void onPostExecute(ForecastBean result) {
        Log.d("WeatherTask", result.toString());
    }
}
