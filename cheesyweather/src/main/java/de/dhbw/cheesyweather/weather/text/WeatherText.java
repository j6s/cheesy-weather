package de.dhbw.cheesyweather.weather.text;

import de.dhbw.cheesyweather.weather.beans.AbstractForecastDataBean;

/**
 * WeatherText:
 * A object which holds an array of texts together with a decison.
 * The decision determines wether or not the texts are applicable
 */
public class WeatherText {

    /**
     * The texts: One of these will be randomly selected if the decision is true
     */
    private String[] texts;

    /**
     * WeatherTextDecision: If it returns true, the texts are applicable
     */
    private WeatherTextDecision decision;

    WeatherText(String[] texts, WeatherTextDecision decision) {
        this.texts = texts;
        this.decision = decision;
    }

    /**
     * Getting the text: Passes the given forecastBean to the decider.
     * If the decider returns tue, getText() returns a random String from within this.texts
     * If it returns false, getText() returns null
     */
    public String getText(AbstractForecastDataBean data) {
        if(this.decision.decide(data)) {
            int rand = (int) Math.round(Math.random() * (this.texts.length - 1));
            return this.texts[rand];
        }
        return null;
    }
}
